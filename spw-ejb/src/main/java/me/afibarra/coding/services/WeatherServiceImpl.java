package me.afibarra.coding.services;

import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

@Stateless
public class WeatherServiceImpl implements WeatherService {
    private static final String EXTERNAL_API_URL = "http://api.openweathermap.org/data/2.5/weather?lat=33.924271&lon=-84.37854&units=imperial&appid=073265414e5557cafc486c185075b1b5";

    @Override
    public String getWeather() {
        Client client = ClientBuilder.newClient();
        String response = client.target(EXTERNAL_API_URL).request("application/json").get(String.class);

        return response;
    }
}
