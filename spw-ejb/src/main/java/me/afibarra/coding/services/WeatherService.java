package me.afibarra.coding.services;

import javax.ejb.Local;

@Local
public interface WeatherService {
    String getWeather();
}
