package me.afibarra.coding.resources;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import me.afibarra.coding.model.WeatherResponse;
import me.afibarra.coding.services.WeatherService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.*;

@Path("spw")
public class WeatherResource {

    @Inject
    private WeatherService weatherService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public WeatherResponse getWeather() throws IOException {
        String externalApiResponse = weatherService.getWeather();
        WeatherResponse response = buildResponse(externalApiResponse);

        return response;
    }

    private WeatherResponse buildResponse(String externalApiResponse) throws IOException {
        WeatherResponse response = new WeatherResponse();

        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> jsonResponse = objectMapper.readValue(externalApiResponse, new TypeReference<Map<String, Object>>() {
        });

        List weatherNodes = (ArrayList) jsonResponse.get("weather");
        HashMap map = (LinkedHashMap) weatherNodes.get(0);
        response.setWeatherDescription((String) map.get("description"));

        LinkedHashMap mainNodes = (LinkedHashMap) jsonResponse.get("main");
        response.setTemperature(String.valueOf(mainNodes.get("temp")));
        response.setMinTemperature(String.valueOf(mainNodes.get("temp_min")));
        response.setMaxTemperature(String.valueOf(mainNodes.get("temp_max")));

        response.setCityName(String.valueOf(jsonResponse.get("name")));

        return response;
    }
}
