package me.afibarra.coding.resources;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("api")
public class SystemApplication extends Application {
}
